import router from "@/router";
import store from "@/store";
import { getters } from "@/store/auth/constants";

export const AuthGuard = async () => {
  const isAuthenticated = store.getters[getters.IS_AUTHENTICATED_GETTER];

  if (!isAuthenticated) {
    router.push({ name: "sign-in" });
  }

  return;
};

export const AdminGuard = async () => {
  const isAdmin = store.getters[getters.IS_ADMIN_GETTER];

  if (!isAdmin) {
    router.push({ name: "dashboard" });
  }

  return;
};

export const AdminTeacherGuard = async () => {
  const isAdmin = store.getters[getters.IS_ADMIN_GETTER];
  const isTeacher = store.getters[getters.IS_TEACHER_GETTER];

  if (!isAdmin || !isTeacher) {
    router.push({ name: "dashboard" });
  }

  return;
};
