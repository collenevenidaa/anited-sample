export const GENDERS = [
  {
    value: "male",
    text: "Male",
  },
  {
    value: "female",
    text: "Female",
  },
  {
    value: "not_to_say",
    text: "Prefer not to say",
  },
];
