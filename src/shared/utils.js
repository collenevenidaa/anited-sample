import { assign, isPlainObject, reduce } from "lodash";

export const deepConvertObjToArray = (obj) => {
  return reduce(
    obj,
    (result, val, key) => {
      const values = reduce(
        val,
        (deepResult, deepVal, deepKey) => {
          if (isPlainObject(deepVal)) {
            assign(deepResult, {
              [deepKey]: deepConvertObjToArray(deepVal),
            });
          } else {
            assign(deepResult, { [deepKey]: deepVal });
          }

          return deepResult;
        },
        {}
      );

      result.push({
        ...values,
        id: key,
      });

      return result;
    },
    []
  );
};
