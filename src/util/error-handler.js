import { actions as SnackBarActions } from "@/store/snackbar/constants";
import store from "@/store";

// https://firebase.google.com/docs/auth/admin/errors

export const errorHandler = async (error) => {
  let text = "Something went wrong!";
  console.log(error.code);
  switch (error.code) {
    case "auth/user-not-found":
      text = "User doesn't exist";
      break;
    case "auth/email-already-exists":
      text = "Oops! This email already exists.";
      break;
    case "auth/invalid-email":
      text = "Invalid email.";
      break;
    case "auth/invalid-password":
    case "auth/invalid-password-hash":
    case "auth/wrong-password":
      text = "Invalid password";
      break;
    case "auth/user-disabled":
      text =
        "Your account is disabled. Contact your administrator to enable your account.";
      break;
    case "auth/email-already-in-use":
      text = "Oops! Email is already in use.";
      break;
    default:
      text = "Something went wrong!";
      break;
    // to be finished
  }

  store.dispatch(SnackBarActions.SHOW_SNACKBAR, {
    text: text,
    color: "danger",
    timeout: 5000,
  });
};
