import { firebaseConfig, getCurrentUser, getData } from "@/firebase";
import { deepConvertObjToArray } from "@/shared/utils";
import { uuidv4 } from "@firebase/util";
import {
  child,
  getDatabase,
  push,
  ref,
  remove,
  serverTimestamp,
  set,
  update,
} from "firebase/database";
import {
  deleteObject,
  getDownloadURL,
  getStorage,
  ref as storageFirebaseRef,
  uploadBytesResumable,
} from "firebase/storage";
import { each, size } from "lodash";

const dbRef = ref(getDatabase(firebaseConfig));
const storage = getStorage();

class ModuleService {
  async getModule() {
    const snapshot = await getData(child(dbRef, "module"));

    return snapshot.val();
  }

  async getResource(contentId, id) {
    const snapshot = await getData(
      child(dbRef, "module-contents/" + contentId + "/resources/" + id)
    );

    return snapshot.val();
  }

  async getModuleContents() {
    const snapshot = await getData(child(dbRef, "module-contents"));

    return deepConvertObjToArray(snapshot.val());
  }

  async getResourceFiles(contentId, id) {
    const snapshot = await getData(
      child(
        dbRef,
        "module-contents/" + contentId + "/resources/" + id + "/files"
      )
    );

    return deepConvertObjToArray(snapshot.val());
  }

  async getStudentSubmissions(contentId, id) {
    const snapshot = await getData(
      child(
        dbRef,
        "module-contents/" + contentId + "/resources/" + id + "/submissions"
      )
    );

    return deepConvertObjToArray(snapshot.val());
  }

  async getUserFiles(contentId, id, userId) {
    const snapshot = await getData(
      child(
        dbRef,
        "module-contents/" +
          contentId +
          "/resources/" +
          id +
          "/submissions/" +
          userId
      )
    );

    return snapshot.val();
  }

  async saveModule(data) {
    update(child(dbRef, "module"), {
      ...data,
      updated_at: serverTimestamp(),
    });
  }

  async addContent(data) {
    push(child(dbRef, "module-contents"), {
      ...data,
      created_at: serverTimestamp(),
      updated_at: serverTimestamp(),
    });
  }

  async addFileResource(data, file, id) {
    const fileName = uuidv4() + "." + file.name.split(".").pop();
    const storageRef = storageFirebaseRef(storage, "contents/" + fileName);
    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log("Upload is " + progress + "% done");
        switch (snapshot.state) {
          case "paused":
            console.log("Upload is paused");
            break;
          case "running":
            console.log("Upload is running");
            break;
        }
      },
      (error) => {
        console.log("ERROR", error);
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          push(child(dbRef, "module-contents/" + id + "/resources"), {
            ...data,
            file_name: fileName,
            url,
            created_at: serverTimestamp(),
            updated_at: serverTimestamp(),
          });
        });
      }
    );
  }

  async addResource(data, id) {
    push(child(dbRef, "module-contents/" + id + "/resources"), {
      ...data,
      created_at: serverTimestamp(),
      updated_at: serverTimestamp(),
    });
  }

  async editContent(id, data) {
    update(child(dbRef, `module-contents/${id}`), {
      ...data,
      updated_at: serverTimestamp(),
    });
  }

  async deleteOutline(data) {
    remove(child(dbRef, `module-contents/${data.id}`));

    if (size(data.resources)) {
      each(data.resources, (resource) => {
        deleteObject(
          storageFirebaseRef(storage, "contents/" + resource.file_name)
        );
      });
    }
  }

  async deleteResource(id, data) {
    remove(child(dbRef, `module-contents/${id}/resources/${data.id}`));

    const storageRef = storageFirebaseRef(
      storage,
      "contents/" + data.file_name
    );

    deleteObject(storageRef);
  }

  async saveResource(contentId, id, data) {
    update(child(dbRef, `module-contents/${contentId}/resources/${id}`), {
      ...data,
      updated_at: serverTimestamp(),
    });
  }

  async addContentResourceFile(data, file, contentId, id) {
    const fileName = uuidv4() + "." + file.name.split(".").pop();
    const storageRef = storageFirebaseRef(storage, "contents/" + fileName);
    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log("Upload is " + progress + "% done");
        switch (snapshot.state) {
          case "paused":
            console.log("Upload is paused");
            break;
          case "running":
            console.log("Upload is running");
            break;
        }
      },
      (error) => {
        console.log("ERROR", error);
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          push(
            child(dbRef, `module-contents/${contentId}/resources/${id}/files`),
            {
              ...data,
              file_name: fileName,
              url,
              created_at: serverTimestamp(),
              updated_at: serverTimestamp(),
            }
          );
        });
      }
    );
  }

  async deleteResourceFile(contentId, id, data) {
    remove(
      child(
        dbRef,
        `module-contents/${contentId}/resources/${id}/files/${data.id}`
      )
    );

    const storageRef = storageFirebaseRef(
      storage,
      "contents/" + data.file_name
    );

    deleteObject(storageRef);
  }

  async addSubmissionFile(file, contentId, id, userId) {
    const fileName = uuidv4() + "." + file.name.split(".").pop();
    const storageRef = storageFirebaseRef(storage, "contents/" + fileName);
    const uploadTask = uploadBytesResumable(storageRef, file);

    const user = getCurrentUser();

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log("Upload is " + progress + "% done");
        switch (snapshot.state) {
          case "paused":
            console.log("Upload is paused");
            break;
          case "running":
            console.log("Upload is running");
            break;
        }
      },
      (error) => {
        console.log("ERROR", error);
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          set(
            child(
              dbRef,
              `module-contents/${contentId}/resources/${id}/submissions/${userId}`
            ),
            {
              user_name: user.displayName,
              score: 0,
              display_name: file.name,
              file_name: fileName,
              url,
              created_at: serverTimestamp(),
              updated_at: serverTimestamp(),
            }
          );
        });
      }
    );
  }

  async saveScore(contentId, id, data) {
    update(
      child(
        dbRef,
        `module-contents/${contentId}/resources/${id}/submissions/${data.id}`
      ),
      {
        score: data.score,
        updated_at: serverTimestamp(),
      }
    );
  }
}

export default new ModuleService();
