import { registerPlugins } from "@/plugins";
import store from "@/store";
import { QuillEditor } from "@vueup/vue-quill";
import "@vueup/vue-quill/dist/vue-quill.snow.css";
import { createApp } from "vue";
import App from "./App.vue";
import { firebaseConfig } from "./firebase";

const app = createApp(App);

app.use(store);
app.component("QuillEditor", QuillEditor);

registerPlugins(app);
app.mount("#app");

//initialize firebase
firebaseConfig;
