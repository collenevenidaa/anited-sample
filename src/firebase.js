import { initializeApp } from "firebase/app";
import {
  createUserWithEmailAndPassword,
  getAuth,
  sendEmailVerification,
  signInWithEmailAndPassword,
  signOut,
  updateProfile,
  EmailAuthProvider,
  reauthenticateWithCredential,
  updatePassword,
  updateEmail,
  sendPasswordResetEmail,
  deleteUser,
} from "firebase/auth";
import {
  child,
  getDatabase,
  onValue,
  push,
  ref,
  remove,
  serverTimestamp,
  update,
} from "firebase/database";
import { errorHandler } from "./util/error-handler";

const config = {
  apiKey: "AIzaSyCOcYaKS6plrGQDv2aDao65SQMXHhSRPes",
  authDomain: "anited-6c27a.firebaseapp.com",
  databaseURL:
    "https://anited-6c27a-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "anited-6c27a",
  storageBucket: "anited-6c27a.appspot.com",
  messagingSenderId: "48345478453",
  appId: "1:48345478453:web:4692b0549b0f7c9a40487c",
  measurementId: "G-2PCVENBVDE",
};

export const firebaseConfig = initializeApp(config);
export const secondFirebase = initializeApp(config, "secondary");

const dbRef = ref(getDatabase(firebaseConfig));

export const getData = (ref) => {
  return new Promise((resolve, reject) => {
    const onError = (error) => {
      reject(error);
    };

    const onData = (snap) => {
      resolve(snap);
    };

    onValue(ref, onData, onError, { onlyOnce: true });
  });
};

export const getDataFromFirebase = async (tableName) => {
  const snapshot = await getData(child(dbRef, tableName));
  const tableArray = [];

  snapshot.forEach((doc) => {
    let item = doc.val();
    item.id = doc.key;

    tableArray.push(item);
  });

  return tableArray;
};

export const getObjectDataFromFirebase = async (tableName) => {
  const snapshot = await getData(child(dbRef, tableName));

  return snapshot.val();
};

export const getDataFromId = async (tableName, id) => {
  const snapshot = await getData(child(dbRef, tableName + id));

  return snapshot.val();
};

export const getArrayDataFromId = async (tableName, id) => {
  const snapshot = await getData(child(dbRef, tableName + id));

  const tableArray = [];

  snapshot.forEach((doc) => {
    let item = doc.val();
    item.id = doc.key;

    tableArray.push(item);
  });

  return tableArray;
};

export const insert = async (tableName, data) => {
  push(child(dbRef, tableName), data);
};

export const updateData = async (tableName, id, data) => {
  update(child(dbRef, tableName + id), data);
};

export const updateTableData = async (tableName, data) => {
  update(child(dbRef, tableName), data);
};

export const deleteData = async (tableName, id) => {
  remove(child(dbRef, tableName + id));
};

// to move in specific service
export const checkKey = async (tableName, id, key) => {
  const snapshot = await getData(child(dbRef, tableName + id));

  const course = snapshot.val();
  const isValidKey = course.key === key;

  // static user id
  const userId = "01";

  if (isValidKey) {
    update(child(dbRef, "user-courses/" + userId), {
      [id]: {
        created_at: serverTimestamp(),
      },
    });
  }

  return isValidKey;
};

// Authentications
const auth = getAuth(firebaseConfig);
const secondAuth = getAuth(secondFirebase);

export const getCurrentUser = () => {
  return auth.currentUser;
};

export const signUpStudent = async (email, password, otherInfo) => {
  try {
    const result = await createUserWithEmailAndPassword(auth, email, password);
    await updateProfile(auth.currentUser, { displayName: otherInfo.name });

    await sendEmailVerification(result.user);

    update(child(dbRef, "user-profiles/" + result.user.uid), {
      isTeacher: false,
      isAdmin: false,
      id: result.user.uid,
      email: result.user.email,
      displayName: result.user.displayName,
      gender: otherInfo.gender,
      creationTime: result.user.metadata.creationTime,
      lastSignInTime: result.user.metadata.lastSignInTime,
    });

    return result;
  } catch (error) {
    console.log(error);
  }
};

export const signIn = async (email, password) => {
  return await signInWithEmailAndPassword(auth, email, password).then(
    (result) => {
      update(child(dbRef, "user-profiles/" + result.user.uid), {
        lastSignInTime: result.user.metadata.lastSignInTime,
      });

      return result;
    }
  );
};

export const logout = async () => {
  return await signOut(auth);
};

export const reauthenticateLoggedInUser = async (userInputOldPassword) => {
  const credential = EmailAuthProvider.credential(
    auth.currentUser.email,
    userInputOldPassword
  );

  const result = await reauthenticateWithCredential(
    auth.currentUser,
    credential
  ).catch((error) => {
    console.log(error.code);
  });

  return result;
};

export const changeCurrentUserPassword = async (newPassword) => {
  return await updatePassword(auth.currentUser, newPassword);
};

export const updateCurrentUserProfile = async (payload) => {
  await updateProfile(auth.currentUser, { displayName: payload.displayName });

  update(child(dbRef, "user-profiles/" + auth.currentUser.uid), {
    ...payload,
  });

  return await getDataFromId("user-profiles/", auth.currentUser.uid);
};

export const changeEmail = async (newEmail) => {
  try {
    await updateEmail(auth.currentUser, newEmail);
    await update(child(dbRef, "user-profiles/" + auth.currentUser.uid), {
      email: newEmail,
    });

    return await getDataFromId("user-profiles/", auth.currentUser.uid);
  } catch (error) {
    console.log(error);
  }
};

export const sendPasswordReset = async (email) => {
  try {
    return await sendPasswordResetEmail(auth, email);
  } catch (error) {
    console.log(error);
  }
};

export const deleteAccount = async (password) => {
  try {
    await reauthenticateLoggedInUser(password);
    await deleteUser(auth.currentUser);

    await deleteData("user-profiles/", auth.currentUser.uid);
  } catch (error) {
    errorHandler(error);
  }
};

export const createUserAsAdmin = async (email, password, otherInfo) => {
  try {
    const result = await createUserWithEmailAndPassword(
      secondAuth,
      email,
      password
    );
    await updateProfile(secondAuth.currentUser, {
      displayName: otherInfo.name,
    });

    await sendEmailVerification(result.user);

    update(child(dbRef, "user-profiles/" + result.user.uid), {
      isTeacher: otherInfo.isTeacher,
      isAdmin: otherInfo.isAdmin,
      id: result.user.uid,
      email: result.user.email,
      displayName: result.user.displayName,
      gender: otherInfo.gender,
      creationTime: result.user.metadata.creationTime,
      lastSignInTime: result.user.metadata.lastSignInTime,
    });

    await signOut(secondAuth);

    return await getDataFromId("user-profiles/", result.user.uid);
  } catch (error) {
    console.log(error);
  }
};
