import auth from "@/store/auth";
import course from "@/store/course";
import snackbar from "@/store/snackbar";
import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";

export default createStore({
  modules: {
    auth,
    course,
    snackbar,
  },

  plugins: [createPersistedState()],

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: true,
});
