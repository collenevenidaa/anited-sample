import { mutations } from "./constants";

export default {
  [mutations.SHOW_SNACKBAR]: (state, payload) => {
    state.text = payload.text;
    state.color = payload.color;
    state.timeout = payload.timeout;
  },
};
