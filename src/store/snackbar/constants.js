export const actions = {
  SHOW_SNACKBAR: "[SnackBar - Action] Show snackbar",
};

export const mutations = {
  SHOW_SNACKBAR: "[Snackbar - Mutation] Show snackbar",
};

export const getters = {};
