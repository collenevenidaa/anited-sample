import { actions, mutations as SnackbarMutations } from "./constants";

export default {
  [actions.SHOW_SNACKBAR]: async ({ commit }, payload) => {
    commit(SnackbarMutations.SHOW_SNACKBAR, payload);
  },
};
