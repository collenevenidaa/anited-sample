import { get } from "lodash";
import { getters } from "./constants";

export default {
  [getters.TOKEN_GETTER]: (state) => {
    return state.token;
  },

  [getters.IS_AUTHENTICATED_GETTER]: (state) => {
    return state.isAuthenticated;
  },

  [getters.GET_PROFILE_GETTER]: (state) => {
    return state.user;
  },

  [getters.IS_TEACHER_GETTER]: (state) => {
    return get(state, "user.isTeacher", false);
  },

  [getters.IS_ADMIN_GETTER]: (state) => {
    return get(state, "user.isAdmin", false);
  },
};
