export function getDefaultState() {
  return {
    text: "",
    color: "",
    timeout: 5000,
  };
}

export default function () {
  return getDefaultState();
}
