export const actions = {
  FETCH_MODULE_ACTION: "[Course - Action] Get module",
};

export const mutations = {
  COURSE_MUTATION: "[Course - Mutation] Course",
};

export const getters = {
  MODULE_GETTER: "[Course - Getter] Get module",
};
