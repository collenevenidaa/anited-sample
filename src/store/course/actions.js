import { getObjectDataFromFirebase } from "@/firebase";
import { actions, mutations as CourseMutations } from "./constants";

export default {
  [actions.FETCH_MODULE_ACTION]: async ({ commit }) => {
    const module = await getObjectDataFromFirebase("module");

    commit(CourseMutations.COURSE_MUTATION, {
      module,
    });
  },
};
