import { mutations } from "./constants";

export default {
  [mutations.COURSE_MUTATION]: (state, { module }) => {
    state.module = module;
  },
};
