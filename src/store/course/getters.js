import { getters } from "./constants";

export default {
  [getters.MODULE_GETTER]: (state) => {
    return state.module;
  },
};
