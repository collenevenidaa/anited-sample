export const actions = {
  LOGIN_ACTION: "[Auth - Action] Login user",
  LOGOUT_ACTION: "[Auth - Action] Logout user",
  SIGNUP_STUDENT_ACTION: "[Auth - Action] Signup student user",
  CHANGE_MY_PASSWORD: "[Auth - Action] Change my password",
  UPDATE_PROFILE: "[Auth - Action] Update Profile",
  CHANGE_EMAIL: "[Auth - Action] Change Email",
};

export const mutations = {
  LOGIN_MUTATION: "[Auth - Mutation] Login user",
  LOGOUT_MUTATION: "[Auth - Mutation] Logout user",
  UPDATE_USER: "[Auth - Mutation] Update user",
};

export const getters = {
  TOKEN_GETTER: "[Auth - Getter] Get token",
  IS_AUTHENTICATED_GETTER: "[Auth - Getter] Get isAuthenticated",
  GET_PROFILE_GETTER: "[Auth - Getter] Get user profile",
  IS_ADMIN_GETTER: "[Auth - Getter] Is user admin",
  IS_TEACHER_GETTER: "[Auth - Getter] Is user teacher",
  GET_UID: "[Auth - Getter] Get UID of current user",
};
