export function getDefaultState() {
  return {
    token: "",
    user: {},
    isAuthenticated: false,
  };
}

export default function () {
  return getDefaultState();
}
