import _ from "lodash";

import { getDefaultState } from "./state";
import { mutations } from "./constants";

export default {
  [mutations.LOGIN_MUTATION]: (state, { token, user }) => {
    state.token = token;
    state.isAuthenticated = true;
    state.user = user;
  },

  [mutations.LOGOUT_MUTATION]: (state) => {
    Object.assign(state, { ...getDefaultState() });
  },

  [mutations.UPDATE_USER]: (state, { user }) => {
    state.user = user;
  },
};
