import {
  changeCurrentUserPassword,
  changeEmail,
  getDataFromId,
  logout,
  reauthenticateLoggedInUser,
  signIn,
  signUpStudent,
  updateCurrentUserProfile,
} from "@/firebase";
import { actions, mutations as AuthMutations } from "./constants";
import { actions as SnackBarActions } from "@/store/snackbar/constants";
import { errorHandler } from "@/util/error-handler";

export default {
  [actions.LOGIN_ACTION]: async ({ commit }, { email, password }) => {
    try {
      const result = await signIn(email, password);
      const profile = await getDataFromId("user-profiles/", result.user.uid);

      commit(AuthMutations.LOGIN_MUTATION, {
        token: result.user.accessToken,
        user: profile,
      });

      return Boolean(result);
    } catch (error) {
      errorHandler(error);
    }
  },

  [actions.LOGOUT_ACTION]: async ({ commit }) => {
    try {
      await logout();
      commit(AuthMutations.LOGOUT_MUTATION);
    } catch (error) {
      await errorHandler(error);
    }
  },

  [actions.SIGNUP_STUDENT_ACTION]: async ({ commit }, { user }) => {
    try {
      const result = await signUpStudent(user.email, user.password, {
        name: user.name,
        gender: user.gender,
      });
      const profile = await getDataFromId("user-profiles/", result.user.uid);

      commit(AuthMutations.LOGIN_MUTATION, {
        token: result.user.accessToken,
        user: profile,
      });
    } catch (error) {
      errorHandler(error);
    }
  },

  [actions.CHANGE_MY_PASSWORD]: async (
    { dispatch },
    { currentPassword, newPassword }
  ) => {
    try {
      const result = await reauthenticateLoggedInUser(currentPassword);

      if (result) {
        await changeCurrentUserPassword(newPassword);

        dispatch(SnackBarActions.SHOW_SNACKBAR, {
          text: "Password updated!",
          color: "primary",
          timeout: 5000,
        });
      }
    } catch (error) {
      errorHandler(error);
    }
  },

  [actions.UPDATE_PROFILE]: async ({ commit, dispatch }, { payload }) => {
    try {
      const newProfile = await updateCurrentUserProfile(payload);

      commit(AuthMutations.UPDATE_USER, { user: newProfile });

      dispatch(SnackBarActions.SHOW_SNACKBAR, {
        text: "Profile updated!",
        color: "primary",
        timeout: 5000,
      });
    } catch (error) {
      errorHandler(error);
    }
  },

  [actions.CHANGE_EMAIL]: async ({ commit, dispatch }, { newEmail }) => {
    try {
      const result = await changeEmail(newEmail);

      if (result) {
        commit(AuthMutations.UPDATE_USER, { user: result });
        dispatch(SnackBarActions.SHOW_SNACKBAR, {
          text: "Email updated!",
          color: "primary",
          timeout: 5000,
        });
      }
    } catch (error) {
      errorHandler(error);
    }
  },
};
