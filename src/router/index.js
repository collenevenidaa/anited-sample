// Composables
import { AdminGuard, AuthGuard } from "@/guards/auth.guard";
import Users from "@/views/admin-pages/Users.vue";
import ChangeEmail from "@/views/auth/ChangeEmail.vue";
import ContactUs from "@/views/ContactUs.vue";
import ChangePassword from "@/views/auth/ChangePassword.vue";
import EditProfile from "@/views/auth/EditProfile.vue";
import ForgotPassword from "@/views/auth/ForgotPassword.vue";
import Profile from "@/views/auth/ProfileView.vue";
import SignIn from "@/views/auth/SignIn.vue";
import SignUp from "@/views/auth/SignUp.vue";
import ModuleResourceView from "@/views/module/ModuleResourceView.vue";
import ModuleView from "@/views/module/ModuleView.vue";
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "/",
        name: "home",
        component: () => import("@/views/DashboardPage.vue"),
      },
      {
        name: "dashboard",
        path: "/dashboard",
        beforeEnter: [AuthGuard],
        component: () => import("@/views/HomePage.vue"),
      },
      {
        name: "sign-up",
        path: "sign-up",
        component: SignUp,
      },
      {
        name: "sign-in",
        path: "sign-in",
        component: SignIn,
      },
      {
        name: "forgot-password",
        path: "forgot-password",
        component: ForgotPassword,
      },
      {
        name: "users",
        path: "users",
        beforeEnter: [AuthGuard, AdminGuard],
        component: Users,
      },
      {
        name: "module",
        path: "module",
        beforeEnter: [AuthGuard],
        component: ModuleView,
      },
      {
        name: "module-resource-view",
        path: "module/resource/view/:id",
        beforeEnter: [AuthGuard],
        component: ModuleResourceView,
      },
      {
        name: "profile",
        path: "profile",
        beforeEnter: [AuthGuard],
        component: Profile,
      },
      {
        name: "edit-profile",
        path: "edit-profile",
        beforeEnter: [AuthGuard],
        component: EditProfile,
      },
      {
        name: "change-password",
        path: "change-password",
        beforeEnter: [AuthGuard],
        component: ChangePassword,
      },
      {
        name: "change-email",
        path: "change-email",
        beforeEnter: [AuthGuard],
        component: ChangeEmail,
      },
      {
        name: "contact-us",
        path: "contact-us",
        component: ContactUs,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
